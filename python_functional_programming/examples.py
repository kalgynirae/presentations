############################################################
from math import floor, sqrt

def is_prime1(n):
    if n < 2:
        return False
    for x in range(2, floor(sqrt(n)) + 1):
        if n % x == 0:
            return False
    return True


def is_prime(n):
    if n < 2:
        return False
    return not any(n % x == 0 for x in range(2, floor(sqrt(n)) + 1))





def is_prime2(n):
    numbers_to_test = range(2, floor(sqrt(n)) + 1)
    return not any(n % x == 0 for x in numbers_to_test)












# The previous versions tested more divisors than necessary. Really,
# we only need to test for divisibility by primes. Let's assume we
# have an efficient prime number generator. How could we make use of
# it?
from itertools import count, takewhile
def primes(max=None):
    """Generate prime numbers

    Stolen from
    http://code.activestate.com/recipes/117119-sieve-of-eratosthenes/
    """
    yield 2
    D = {}
    for q in count(3, 2):
        if max and q > max:
            raise StopIteration
        p = D.pop(q, None)
        if p is None:
            # q is prime
            yield q
            D[q * q] = 2 * q
        else:
            # q is composite
            x = p + q
            while x in D:
                x += p
            D[x] = p




def is_prime3(n):
    if n < 2: return False
    return not any(n % x == 0 for x in primes(sqrt(n)))









def is_prime4(n):
    return n in primes(n)
    # Turns out this one is slow. Like really slow.







############################################################
# Sum of first 10 squares
def sum_example():
    return sum(n**2 for n in range(10))













filename = sys.argv[1]
line_number = sys.argv[2]
f = open(filename)
for _ in range(line_number - 1):
    f.readline()
print(f.readline())

















############################################################
# Printing a specific line of a file
def nth(n, iterable):
    i = iter(iterable)
    for _ in range(n): next(i)
    return next(i)

def nth_line_of_file(n, file):
    return nth(n, open(file))



nth(5, ...)

def get_5th(x):
    return nth(5, x)

from functools import partial

get_5th = partial(nth, 5)
get_5th

nth 5














############################################################
def quicksort(s):
    if len(s) == 0: return []
    pivot, *rest = s
    smaller = quicksort(x for x in rest if x <= pivot)
    larger = quicksort(x for x in rest if x > pivot)
    return smaller + [pivot] + larger









############################################################
# Sort lines of text by the value of the number in the third
# column.
# See https://www.reddit.com/r/learnprogramming/comments/2w08dy/python_sort_lines_in_file_by_part_of_string/
...














############################################################
# sort <filename | uniq

def dedup(lines):
    return sorted(set(lines))











############################################################
# Given a text file and an integer k, print the k most
# common words in the file and the number of their
# occurrences in decreasing frequency.
# tr -cs A-Za-z '\n' | tr A-Z a-z | sort | uniq -c | sort -rn | sed ${1}q
...












############################################################
# Iterate over all possible sublists
def sublists(s):
    length = len(s)
    for size in range(1, length + 1):
        for start in range(0, (length - size) + 1):
            yield s[start:start+size]

print(list(sublists([1, 2, 3, 4, 5])))













############################################################
# Count words
def word_count(s):
    return len(str.split(s))




def compose(f, g):
    def composed(x):
        return f(g(x))
    return composed




word_count = compose(len, str.split)


###########################################################
# Read text1 and record the number of words per line.
# Print the words of text2 in the recorded layout (same
# number of words per line).
# https://www.reddit.com/r/learnpython/comments/2vvv8k/pythonify_this_little_code/

def format_weirdly(text1: 5, text2: 5) -> 'fjskflsf':
    word_counts = map(word_count, text1.splitlines())
    words = iter(text2.split())
    for count in word_counts:
        yield ' '.join(next(words) for _ in range(count))

print('\n'.join(format_weirdly(...)))












###########################################################
# Remove whitespace from a string (or file)
# tr -d '[:space:]'
remove_whitespace = compose(''.join, str.split)








import contextlib
import time

@contextlib.contextmanager
def tell_me_when_its_done():
    print('entering the block')
    yield
    print('exiting the block')


with tell_me_when_its_done():
    time.sleep(5)
    ...





