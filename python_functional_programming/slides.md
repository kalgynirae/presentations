%title: Functional programming in Python
%author: Colin Chan
%date: 2015-02-18
% vim: colorcolumn=60

-> # About Colin Chan

## Qualifications

* has lots of Reddit points
* plays the carillon
* plays the pipe organ
* plays the piano
* likes Indian food
* enjoys cooking
* Facebook
* is good at Python
* is good at fixing stuff
* is good at being a wizard

---

-> # Functional Programming

## What is it?

* A *programming paradigm* that encourages:
    * pure functions (no side effects)
    * immutable data structures
    * higher-order functions
        * requires first-class functions
    * recursion

---

-> # Functional Programming

## What are the advantages?

* Well-written functional code is
    * easy to test
    * modular
    * reusable
    * easier to prove correct

---

-> # Functional Programming

## Are there disadvantages?

* Some problems are easier to solve with another paradigm
* Functional code is often not as performant as
  well-written imperative code

---

-> # Functional Programming

## What makes a function pure?

* Execution produces no side effects
    * no I/O
* Same inputs always produce the same outputs
* Good for parallel/concurrent execution

---

-> # FP in Python

## Is Python a purely functional language?

* of course not
* Python has
    * assignments
    * loops
    * etc.

* However, some of Python's features are quite functional
    * first-class functions (and `lambda` functions)
    * iterators (and generators, and the `itertools` module)
    * decorators

* Successful Python code is usually a mixture of functional
  and OOP

---

-> # FP in Python

## General guidelines to FP in Python

* Prefer immutability and pure functions
* Iterate everything
* Use generators when appropriate

---

-> # FP in Python

## Preferring immutability

* Most Python objects are mutable
* We can pretend they are immutable
* Many mutating operations have non-mutating analogues
    * `list.sort` --> `sorted`
    * `list.pop` --> slicing

---

-> # FP in Python

## Iterating everything (1/3)

* Many things in Python are iterable
* Many ways to iterate:
    * `for` loops
    * Container constructors (`list`, `dict`, `set`, `tuple`)
    * `map`, `zip`, `filter`, `sum`, `any`, `all`, `min`, `max`
    * Call `next` on an iterator manually
* Examples!

---

-> # FP in Python

## Iterating everything (2/3)

* Python's iterator protocol is simple
* An iterator must have a `__next__` method which
    * returns the next item each time it is called
    * raises a `StopIteration` exception if no more items
* Use `next` to get the next item from an iterator

---

-> # FP in Python

## Generators


---

-> # FP in Python

## Functions

* Important built-in functions:
    * `map`
    * `zip`
    * `filter`?

---

-> # Examples of FP code

## Summing integers

---

-> # Examples of FP code

## "Recursive" `for` loop

